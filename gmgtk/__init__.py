name = 'gmgtk'

import gi
gi.require_version('Gtk', '3.0')

from gi.repository import Gtk, Gio

class GMApp(Gtk.Application):
    def __init__(self, builder_resource, icon_name, resource_path, app_name='GMGtk Application', **kwargs):
        super().__init__(**kwargs)
        self.resource_path = resource_path
        self.builder = Gtk.Builder.new_from_resource(builder_resource)
        self.builder.connect_signals(self)
        self.window = self.builder.get_object('window')
        self.window.set_icon_name(icon_name)
        self.gtk_settings = Gtk.Settings.get_default()
        self.app_name = app_name

    def do_before_activate(self):
        '''
        Redefine and populate this method with operations you need
        to run before the main window shows
        '''

    def do_activate(self):
        self.add_window(self.window)
        self.window.set_wmclass(self.app_name, self.app_name)
        # self.window.set_title(self.app_name)

        appMenu = Gio.Menu()
        appMenu.append("About", "app.about")
        appMenu.append("Settings", "app.settings")

        about_action = Gio.SimpleAction.new("about", None)
        about_action.connect("activate", self.on_about_activate)

        self.builder.get_object('aboutdialog').connect(
            "delete-event", lambda *_:
                self.builder.get_object("aboutdialog").hide() or True
        )

        self.add_action(about_action)

        settings_action = Gio.SimpleAction.new("settings", None)
        settings_action.connect("activate", self.on_settings_activate)
        self.builder.get_object("settingsWindow").connect(
            "delete-event", lambda *_:
                self.builder.get_object("settingsWindow").hide() or True
        )
        self.add_action(settings_action)

        quit_action = Gio.SimpleAction.new("quit", None)
        quit_action.connect("activate", self.on_quit_activate)
        self.add_action(quit_action)
        self.set_app_menu(appMenu)

        self.do_before_activate() # redefine this to call stuff before the window shows

        self.window.show_all()

    def on_about_activate(self, *args):
        self.builder.get_object("aboutdialog").show()

    def on_settings_activate(self, *args):
        self.builder.get_object("settingsWindow").show()

    def on_aboutdialog_close(self, *args):
        self.builder.get_object("aboutdialog").hide()
        
    def do_before_quit(self):
        '''
        Redefine and populate this method with operations you need
        to run before the app quits
        '''

    def on_quit_activate(self, *args):
        self.do_before_quit() # redefine this to call stuff before the app quits
        self.quit()

    def onDeleteWindow(self, *args):
        self.do_before_quit() # redefine this to call stuff before the app quits
        self.quit()
